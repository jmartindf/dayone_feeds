CREATE TABLE IF NOT EXISTS tweet_history (
	screen_name TEXT,
	tweet_at TEXT,
	id INTEGER PRIMARY KEY ASC,
	data BLOB
);
