#!/usr/bin/env ruby
#
#  Created by Mark James Adams on 2007-05-14.
#  Copyright (c) 2007. All rights reserved.

require 'rubygems'
require 'hpricot'
require 'uri'
require 'fileutils'
require 'optparse'
require 'yaml'
require 'oauth'
require 'sqlite3'
require 'time'

# Exchange your oauth_token and oauth_token_secret for an AccessToken instance.
def prepare_access_token(oauth_token, oauth_token_secret)
	consumer = OAuth::Consumer.new("APIKey", "APISecret",
																 { :site => "http://api.twitter.com",
																	 :scheme => :header
	})
	# now create the access token object from passed values
	token_hash = { :oauth_token => oauth_token,
		:oauth_token_secret => oauth_token_secret
	}
	access_token = OAuth::AccessToken.from_hash(consumer, token_hash )
	return access_token
end

def hark(user, token, secret, db, since_id, page)
  
  # FileUtils.mkdir_p $options[:user] # Create a directory to hold the tweets
  
  listening = true
	ins = "INSERT INTO tweet_history(screen_name, tweet_at, id, data)"
	ins = ins + "VALUES (:screen_name, :tweet_at, :tweet_id, :tweet_xml)"
	stmt = db.prepare(ins)
  
  # Exchange our oauth_token and oauth_token secret for the AccessToken instance.
  access_token = prepare_access_token(token, secret)
  
  while listening do # parse the account archive until we come to the last page (all)
                     # or we see a tweet we've alrady downloaded
  
    begin

      unless since_id.nil? 
        since_id_parameter = "&since_id=#{since_id}"
      else
        since_id_parameter = ""
      end

      page_parameter = "&page=#{page}"
      count_parameter = "count=200"
			screen_name_parameter = "&screen_name="+user

      query = count_parameter + since_id_parameter + page_parameter + screen_name_parameter
      
      user_timeline_url = 'http://api.twitter.com/1/statuses/user_timeline.xml?' + query
      puts "Fetching #{user_timeline_url}"
			user_timeline_resource = access_token.request(:get, user_timeline_url)
      user_timeline_xml = user_timeline_resource.body
			File.open("debug.log", 'w') { |data| data << user_timeline_resource.body }
      puts "Retrieved page #{page} ..."
      
      tweets = (Hpricot(user_timeline_xml)/"status")
      puts "Parsing #{tweets.length} tweets..."

      tweets.each {|tweet|
        id = tweet.at("id").inner_html
        save_tweet(user, id, tweet, db, stmt, access_token)
      }

      unless tweets.empty?
        page = page + 1
      else
        listening = false
      end
     end
  end # listening
  
end

def save_tweet(user, id, tweet, db, stmt, access_token)
  exists = db.get_first_value("select id from tweet_history where id=?", id)
  if exists.nil?
    if tweet.nil?
      tweet_url = "http://api.twitter.com/1/statuses/show/"+id+".xml"
      puts "Retrieving tweet " + id
      tweet_resource = access_token.request(:get, tweet_url)
      tweet_xml = tweet_resource.body
      tweet = (Hpricot(tweet_xml)/"status").first
      puts "Couldn't retrieve tweet "+id unless not tweet.nil?
    end#if
    at = Time.parse(tweet.at("created_at").inner_html).iso8601
    puts "Saving tweet #{id}" 
    stmt.execute({"screen_name"=>user,"tweet_at"=>at, "tweet_id" => id, "tweet_xml"=>tweet.to_s})
    reply_to = tweet.at("in_reply_to_status_id").inner_html
    if not reply_to.nil?
      save_tweet(user, reply_to, nil, db, stmt, access_token)
    end#if reply_to
  end#if exists.nil?
end#def save_tweet

begin

  CONFIG = YAML.load_file('config.yml')
  $options = {}
	$options[:secret] = CONFIG['secret']
	$options[:token] = CONFIG['token']
	$options[:user] = CONFIG['username']
	
  OptionParser.new do |opts|
    opts.banner = "Usage: aviary.rb --updates [new|all] --page XXX"
  
    $options[:updates] = :new
    opts.on("--updates [new|all]", [:new, :all], "Fetch only new or all updates") {|updates| $options[:updates] = updates}
    $options[:page] = 1
    opts.on("--page XXX", Integer, "Page") {|page| $options[:page] = page}
  end.parse!

  if [:updates].map {|opt| $options[opt].nil?}.include?(nil)
    puts "Usage: aviary.rb --updates [new|all] --page XXX"
    exit
  end

  # $statuses = Array.new
  # FileUtils.mkdir_p $options[:user]
  # Dir.new($options[:user]).select {|file| file =~ /\\d+.xml$/}.each{|id_xml| 
  #   $statuses.push(id_xml.gsub('.xml', '').to_i)
  # }

	db = SQLite3::Database.new( "tweetdb.sqlite3" )

  case $options[:updates]
  when :new
    # find the most recent status
    since_id = db.get_first_value("select id from tweet_history order by id desc limit 1")
  else :all
    since_id = nil
  end

  hark($options[:user], $options[:token], $options[:secret], db, since_id, $options[:page])
  
rescue Errno::ENOENT
  puts "Whoops!"
  puts "There is no configuration file."
  puts "Place your username and password in a file called `config.yml`. See config-example.yml."
end



