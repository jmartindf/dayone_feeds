
#!/usr/bin/env ruby
#
#  Created by Mark James Adams on 2007-05-14.
#  Copyright (c) 2007. All rights reserved.

require 'rubygems'
require 'hpricot'
require 'restclient'
require 'uri'
require 'fileutils'
require 'optparse'
require 'yaml'
require 'oauth'


begin

  CONFIG = YAML.load_file('config.yml')
  $options = {}
  $options[:user] = CONFIG['username']
	$options[:secret] = CONFIG['secret']
	$options[:token] = CONFIG['token']
  OptionParser.new do |opts|
    opts.banner = "Usage: aviary.rb --updates [new|all] --page XXX"
  
    $options[:updates] = :new
    opts.on("--updates [new|all]", [:new, :all], "Fetch only new or all updates") {|updates| $options[:updates] = updates}
    $options[:page] = 1
    opts.on("--page XXX", Integer, "Page") {|page| $options[:page] = page}
  end.parse!

  if [:updates].map {|opt| $options[opt].nil?}.include?(nil)
    puts "Usage: aviary.rb --updates [new|all] --page XXX"
    exit
  end

  FileUtils.mkdir_p $options[:user]
  Dir.new($options[:user]).select {|file| file =~ /\d+.xml$/}.each{|id_xml| 
    tweet = (Hpricot(open(id_xml))/"status")
    id = tweet.at("id").inner_html
    save_tweet(user, id, tweet, db, stmt, access_token)

  }

rescue Errno::ENOENT
  puts "Whoops!"
  puts "There is no configuration file."
  puts "Place your username and password in a file called `config.yml`. See config-example.yml."
end



